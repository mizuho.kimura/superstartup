<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class action extends Model
{
    protected $fillable = [
        'date', 'prenom_nom', 'entreprise', 'type_action', 'montant', 'provenance_contact',
        'prochain_action', 'echaence_prochain_action', 'statut_action', 'commentaires'
    ];
}
