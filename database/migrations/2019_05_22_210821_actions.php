<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Actions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('prenom_nom');
            $table->string('entreprise');
            $table->string('type_action');
            $table->bigInteger('montant');
            $table->string('provenance_contact');
            $table->string('prochain_action');
            $table->date('echaence_prochain_action');
            $table->string('statut_action');
            $table->string('commentaires');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actions');
    }
}
